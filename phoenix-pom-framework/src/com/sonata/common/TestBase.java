package com.sonata.common;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.sonata.constants.Constants;
import com.sonata.utility.LocatorSetAndGet;
import com.sonata.utility.ObjectReader;

public class TestBase {

	public ThreadLocal<WebDriver> localDriver;
	public ThreadLocal<ExtentReports> localReport;
	public ThreadLocal<WebDriverWait> localWait;
	public Logger logger;
	public boolean result = true;
	private String className;

	/*
	 * public void setDtails(WebDriver driver,ExtentReports report,Logger
	 * logger){ this.driver = driver; this.report = report; this.logger =
	 * logger; }
	 */

	public TestBase(ThreadLocal<WebDriver> localDriver,
			ThreadLocal<ExtentReports> localReport,
			ThreadLocal<WebDriverWait> localWait, Logger logger) {
		this.localDriver = localDriver;
		this.localReport = localReport;
		this.localWait = localWait;
		this.logger = logger;
	}

	protected void setClassName(String className) {
		this.className = className;
	}

	protected By createByObjectByReplacingGivenText(By object,
			String replaceString, String newString) {
		String objectString = object.toString();
		By by = null;
		try {
			newString = newString.trim();
			objectString = objectString.replace(replaceString, newString)
					.trim();
			if (objectString.contains("By.xpath")) {
				objectString = objectString.replace("By.xpath", "").replace(
						":", "");
				objectString = objectString.trim();
				by = By.xpath(objectString);
			}
			if (objectString.contains("By.css")) {
				objectString = objectString.replace("By.cssSelector", "")
						.replace(":", "");
				objectString = objectString.trim();
				by = By.cssSelector(objectString);
			}
			if (objectString.contains("By.id")) {
				objectString = objectString.replace("By.id", "").replace(":",
						"");
				objectString = objectString.trim();
				by = By.id(objectString);
			}
			if (objectString.contains("By.name")) {
				objectString = objectString.replace("By.name", "").replace(":",
						"");
				objectString = objectString.trim();
				by = By.name(objectString);
			}
			if (objectString.contains("By.linkText")) {
				objectString = objectString.replace("By.linkText", "").replace(
						":", "");
				objectString = objectString.trim();
				by = By.linkText(objectString);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return by;
	}

	private LocatorSetAndGet extractObject(By object) {
		LocatorSetAndGet locObject = new LocatorSetAndGet();
		try {
			ExtentReports report = localReport.get();
			ObjectReader objReader = new ObjectReader(logger, report);
			ConcurrentHashMap<String, By> objectsMap = objReader
					.storeObjects(className);
			for (Map.Entry<String, By> values : objectsMap.entrySet()) {
				By by = values.getValue();
				String objectName = values.getKey();
				if (object.equals(by)) {
					logger.info("Object read:" + objectName + "\t" + by);
					locObject.setLocatorName(objectName);
					locObject.setLocatoreValue(by);
					break;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return locObject;
	}

	public void launchApplication(String url) {
		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			driver.manage().window().maximize();
			driver.get(url);
			waitForPageLoad(localDriver);
			report.log(LogStatus.INFO, "Launching URL:" + url);
			logger.info("Application under test is launching");
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "URL has not launched");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info("URL has not launched **********");
			logger.error(e);
			result = false;
		} finally {
			try {
				Assert.assertTrue(result, "Launch URL");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
	}

	public String getProjectPath() {
		File currentDirFile = new File("");
		String path = currentDirFile.getAbsolutePath();
		return path;
	}

	public boolean isElementPresent(By objectName) {
		boolean isElementDisplayed = false;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			if (driver.findElement(locatorValue).isDisplayed() == true) {
				report.log(LogStatus.INFO, locatorName
						+ " Componet is displayed from " + " page");
				logger.info(locatorValue + " Component is displayed from "
						+ " page");
				isElementDisplayed = true;
			} else {
				report.log(LogStatus.INFO, locatorName + locatorValue
						+ " Component is not displayed from " + " page");
				logger.info(locatorValue + " Component is not displayed from "
						+ " page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

		} catch (Exception e) {
			logger.error(locatorName + " " + locatorValue + "*******"
					+ e.getMessage());
			report.log(LogStatus.INFO, locatorName + " " + locatorValue
					+ " Component is not displayed from " + " page");
		}
		return isElementDisplayed;
	}

	public void performComponentPresent(By objectName) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			if (driver.findElement(locatorValue).isDisplayed() == true) {
				logger.info(locatorName + " component is displayed");
				report.log(LogStatus.PASS, locatorName
						+ " component is dispalyed from " + " page");
			} else {
				report.log(LogStatus.FAIL, locatorName + " " + locatorValue
						+ " component is not dispalyed from " + " page");
				logger.info(locatorName + " " + locatorValue
						+ " component is not dispalyed from " + " page");
				report.attachScreenshot(takeScreenShotExtentReports());
				result = false;
				Assert.assertTrue(result, "Component is not displayed");
			}
		} catch (Exception e) {
			report.log(LogStatus.FAIL, locatorName + " " + locatorValue
					+ " component is not dispalyed from " + " page");
			logger.info(locatorName + " " + locatorValue
					+ " component is not dispalyed **********");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "Component is not displayed");
		}
	}

	public void reportFailure() {
		try {
			ExtentReports report = localReport.get();
			report.log(LogStatus.FAIL, "Test Case verification failed");
			takeScreenShotExtentReports();
			Assert.fail("Test Case verification failed");
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void reportPassed() {
		try {
			ExtentReports report = localReport.get();
			report.log(LogStatus.PASS, "Test Case verification passed");
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void reportNoData() {
		try {
			ExtentReports report = localReport.get();
			logger.info("No data to proceed the execution");
			report.log(LogStatus.INFO, "No data to proceed the execution");
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void stringComparisonWithEquals(String expected, String actual) {
		ExtentReports report = localReport.get();
		try {
			expected = expected.trim();
			actual = actual.trim();
			if (expected.equalsIgnoreCase(actual)) {
				report.log(LogStatus.PASS, "Both strings are equals Expected:"
						+ expected + "\t" + "Actual:" + actual);
				logger.info("Both strings are equals");
				Assert.assertTrue(true, "Both strings are equals");
			} else {
				report.log(LogStatus.FAIL,
						"Both strings are not equals Expected:" + expected
								+ "\t" + "Actual:" + actual);
				logger.info("Both strings are not equals");
				Assert.assertTrue(false, "Both strings are not equals");
			}
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "Both strings are not equals");
			logger.error(e);
			Assert.assertTrue(false);
		}
	}

	public void stringComparisonWithContains(String expected, String actual) {
		ExtentReports report = localReport.get();
		try {
			expected = expected.trim().toLowerCase();
			actual = actual.trim().toLowerCase();
			if (expected.contains(actual)) {
				report.log(LogStatus.PASS, "Both strings are matched Expected:"
						+ expected + "\t" + "Actual:" + actual);
				logger.info("Both strings are matched");
				Assert.assertTrue(true, "Both strings are matched");
			} else {
				report.log(LogStatus.FAIL,
						"Both strings are not matched Expected:" + expected
								+ "\t" + "Actual:" + actual);
				logger.info("Both strings are not matched");
				Assert.assertTrue(false, "Both strings are not matched");
			}
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "Both strings are not matched");
			logger.error(e);
			Assert.assertTrue(false);
		}
	}

	public void waitForPageLoad(ThreadLocal<WebDriver> driver) {
		result = true;
		try {
			ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					return ((JavascriptExecutor) driver).executeScript(
							"return document.readyState").equals("complete");
				}
			};
			try {
				WebDriverWait wait = new WebDriverWait(driver.get(), 40);
				wait.until(pageLoadCondition);
			} catch (Exception e) {
				logger.info("Could not wait until page load condition");
				logger.error(e);
			}
		} catch (Exception e) {
			logger.info("Could not wait for page load ********");
			logger.error(e);
			result = false;
		}
	}

	public void pageLoad() {
		while (true) {
			if (String.valueOf(
					((JavascriptExecutor) localDriver.get())
							.executeScript("return document.readyState"))
					.equals("complete")) {
				break;
			}
		}
	}

	public void tearDownTest() {
		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			if (driver != null) {
				driver.quit();
				report.log(LogStatus.INFO, "Browser has closed successfully");
			}
		} catch (Exception e) {
		} finally {
			if (driver != null) {
				driver.quit();
			}
		}
	}

	public void stopTest() {
		ExtentReports report = localReport.get();
		try {
			report.endTest();
		} catch (Exception e) {
		} finally {
			if (report != null) {
				report.endTest();
			}
		}
	}

	public void performSelectBox(By objectName) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			if (!driver.findElement(locatorValue).isSelected()) {
				driver.findElement(locatorValue).click();
				report.log(LogStatus.INFO, locatorName + " " + "Checkbox/Radio"
						+ " is selected");
				logger.info(locatorName + " " + "Checkbox/Radio"
						+ " is selected");
			}
		} catch (Exception e) {
			logger.info(locatorName + " " + locatorValue
					+ " is not selected **********");
			report.log(LogStatus.ERROR, locatorName + " " + locatorValue + " "
					+ "Checkbox/Radio" + " is not selected");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "Checkbox/Radio button is not selected");
		}
	}

	public void performSelectDropDown(By objectName, String testData) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			Select selectBox = new Select(driver.findElement(locatorValue));
			selectBox.selectByValue(testData);
			report.log(LogStatus.INFO, locatorName + " value is selected as "
					+ testData);
			logger.info(locatorName + " value is selected as " + testData);

		} catch (Exception e) {
			logger.info(locatorName + " " + locatorValue
					+ " Select dropdown is not selected **********" + testData);
			report.log(LogStatus.ERROR, locatorName + " " + locatorValue
					+ " value is not selected as " + testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			Assert.assertTrue(result,
					" Select dropdown is not selected **********");
		}
	}

	public void performMouseOver(By objectName) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			WebElement element = driver.findElement(locatorValue);
			Actions action = new Actions(driver);
			action.moveToElement(element).build().perform();
			report.log(LogStatus.INFO, locatorName + " Mousoever is happened");
			logger.info(locatorName + "  Mousoever is happened");

		} catch (Exception e) {
			report.log(LogStatus.INFO, locatorName + " " + locatorValue
					+ " Mousoever is not happened");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info(locatorName + " " + locatorValue
					+ "  Mousoever is not happened");
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "  Mousoever is not happened");
		}
	}

	public void scrollIntoViewElement(By objectName) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			WebElement element = driver.findElement(locatorValue);
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView(true);", element);
			report.log(LogStatus.INFO, locatorName + " Scrolled into view");
			logger.info(locatorName + " Scrolled into view");
		} catch (Exception e) {
			result = false;
			logger.info(locatorName + " " + locatorValue
					+ " Scrolling to element not happened");
			Assert.assertTrue(result, "Scrolling to element not happened");
		}
	}

	public void scrollWindowBarDown() {
		try {
			WebDriver driver = localDriver.get();
			((JavascriptExecutor) driver).executeScript(
					"window.scrollBy(0,250)", "");
			logger.info("The window bar scrolled");
		} catch (Exception e) {
			logger.info("The window bar is not scrolled");
			logger.error(e);
		}
	}

	public void scrollWindowBarUp() {
		try {
			WebDriver driver = localDriver.get();
			((JavascriptExecutor) driver).executeScript(
					"window.scrollBy(0,-250)", "");
			logger.info("The window bar scrolled");
		} catch (Exception e) {
			logger.info("The window bar is not scrolled");
			logger.error(e);
		}
	}

	public void performSelectFromList(By objectName, String testData) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			List<WebElement> listBox = driver.findElements(locatorValue);
			for (WebElement listValue : listBox) {
				if (listValue.getText().contains(testData)) {
					Actions action = new Actions(driver);
					// Thread.sleep(1000L);
					action.moveToElement(listValue).click().build().perform();
					break;
				}
			}
			report.log(LogStatus.INFO, locatorName + " value is selected as "
					+ testData);
			logger.info(locatorName + " value is selected as " + testData);
		} catch (Exception e) {
			logger.info(locatorName + " " + locatorValue
					+ " could not select value from the List **********");
			report.log(LogStatus.ERROR, locatorName + " " + locatorValue
					+ " value is not selected as " + testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "could not select value from the List");
		}
	}

	public void pageDisplay(String expectedText) {
		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			pageLoad();
			ajaxWait();
			expectedText = expectedText.toLowerCase();
			String pageSource = driver.getPageSource();
			if (pageSource.toLowerCase().contains(expectedText)) {
				report.log(LogStatus.PASS, "Page displayed with expected text");
				logger.info("Page displayed with expected text");
			} else if (pageSource.toLowerCase().contains("all gone")) {
				report.log(LogStatus.FAIL,
						"Page displayed with <b>All Gone</b>");
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.error("Page displayed with <b>All Gone</b>");
				Assert.fail("Page not displayed");
			} else if (pageSource.toLowerCase().contains(
					"service temporarily unavailable")) {
				report.log(LogStatus.FAIL,
						"Page displayed with <b>service temporarily unavailable</b>");
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.error("Page displayed with <b>service temporarily unavailable</b>");
				Assert.fail("Page not displayed");
			} else if (pageSource.toLowerCase().contains(
					"technical difficulties")) {
				report.log(LogStatus.FAIL,
						"Page displayed with <b>Technical Difficulties</b>");
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.error("Page displayed with <b>Technical Difficulties</b>");
				Assert.fail("Page not displayed");
			} else {
				report.log(LogStatus.FAIL, "Page displayed with <b>error</b>");
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.error("Page displayed with <b>error</b>");
				Assert.fail("Page not displayed");
			}
		} catch (Exception e) {
			logger.info("Exception in" + " page display method **********");
			report.log(LogStatus.ERROR, " Page not displayed");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	public void performEnterKey(By objectName) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			driver.findElement(locatorValue).sendKeys(Keys.ENTER);
			report.log(LogStatus.INFO, locatorName
					+ " performed the Enter key operation");
			logger.info(locatorName + " performed the Enter key operation");
		} catch (Exception e) {
			logger.info(locatorName + " " + locatorValue
					+ " could not perform ENTER key operation **********");
			report.log(LogStatus.ERROR, locatorName + " " + locatorValue
					+ " could not perform ENTER key operation");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "could not perform ENTER key operation");
		}
	}

	public void waitForElementToBeDisplayed(By objectName) {
		try {
			WebDriverWait wait = localWait.get();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(objectName));
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void presenceOfElementLocated(By objectName) {
		try {
			WebDriverWait wait = localWait.get();
			wait.until(ExpectedConditions.presenceOfElementLocated(objectName));
			logger.info("Presenece of element:" + objectName);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void invisibilityOfElementLocated(By objectName) {
		try {
			WebDriverWait wait = localWait.get();
			wait.until(ExpectedConditions
					.invisibilityOfElementLocated(objectName));
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void elementToBeSelected(By objectName) {
		try {
			WebDriverWait wait = localWait.get();
			wait.until(ExpectedConditions.elementToBeSelected(objectName));
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void waitForVisibility(By objectName) {
		try {
			WebDriverWait wait = localWait.get();
			WebDriver driver = localDriver.get();

			@SuppressWarnings("unused")
			long start = System.currentTimeMillis();
			((JavascriptExecutor) driver)
					.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 500);");
			// logger.info("Elapsed time: " + (System.currentTimeMillis() -
			// start));
			wait.until(ExpectedConditions.visibilityOf(driver
					.findElement(objectName)));
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void ajaxWait() {
		boolean ajaxIsComplete = false;
		while (true) {
			WebDriver driver = localDriver.get();
			ajaxIsComplete = (boolean) ((JavascriptExecutor) driver)
					.executeScript("return jQuery.active == 0");
			/*
			 * isLoaderHidden = (boolean) ((JavascriptExecutor) driver)
			 * .executeScript("return $('.spinner').is(':visible') == false");
			 */
			/*
			 * if (ajaxIsComplete && isLoaderHidden) {
			 * System.out.println("After ajaxIsComplete:" + ajaxIsComplete);
			 * break; }
			 */
			try {
				if (ajaxIsComplete) {
					driver.findElement(By
							.xpath("//div[contains(@class,'sapUiLocalBusyIndicatorAnimation')]"));
					// Thread.sleep(100);
				}
			} catch (Exception e) {
				break;
			}
		}
	}

	public List<String> performMultipleGetText(By objectName) {
		result = true;
		List<String> textList = Collections
				.synchronizedList(new ArrayList<String>());
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			List<WebElement> element = getMultipleElement(objectName);
			for (WebElement elementText : element) {
				textList.add(elementText.getText().trim());
			}
			logger.info(locatorName + " Multiple text is captured");
			report.log(LogStatus.INFO, " Multiple text is captured");
		} catch (Exception e) {
			report.log(LogStatus.ERROR, locatorName + " " + locatorValue
					+ " could not get the multiple text **********");
			logger.info(locatorName + " " + locatorValue
					+ " could not get the multiple text **********");
			logger.error(e);
			result = false;
			Assert.assertTrue(result, " could not get the multiple text");
		}
		return textList;
	}

	public List<WebElement> getMultipleElement(By objectName) {
		result = true;
		List<WebElement> element = Collections
				.synchronizedList(new ArrayList<WebElement>());
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			element = driver.findElements(locatorValue);
			report.log(LogStatus.INFO, " Web elemenents are captured");
			logger.info(locatorName + " Web elemenents are captured");
		} catch (Exception e) {
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info(locatorName + " " + locatorValue
					+ "  Couldn't get the web elemenents**********");
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "Couldn't get the web elemenents");
		}
		return element;
	}

	public void performClickByJs(By objectName) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();
		try {
			waitForVisibility(locatorValue);
			pageLoad();
			ajaxWait();
			WebDriver driver = localDriver.get();
			WebElement element = driver.findElement(locatorValue);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView(true);", element);
			executor.executeScript("arguments[0].click();", element);
			logger.info(locatorName + " is clicked by using javascript!");
		} catch (Exception e) {
			logger.error(locatorName + " " + locatorValue
					+ "is not clicked by using javascript!");
			result = false;
			Assert.assertTrue(result, "is not clicked by using javascript!");
		}
	}

	public void performUnSelectBox(By objectName) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			if (driver.findElement(locatorValue).isSelected()) {
				driver.findElement(locatorValue).click();
				report.log(LogStatus.INFO, locatorName + " CheckBox"
						+ " is un selected");
				logger.info(locatorName + " CheckBox" + " is un selected");
			}
		} catch (Exception e) {
			logger.info(locatorName + " " + locatorValue
					+ " is not un selected **********");
			report.log(LogStatus.ERROR, locatorName + " CheckBox"
					+ " is not un selected");
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "is not un selected");
		}
	}

	public void performEnterText(By objectName, String testData) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			driver.findElement(locatorValue).clear();
			driver.findElement(locatorValue).sendKeys(testData);
			report.log(LogStatus.INFO, locatorName + " text is set as "
					+ "<strong>" + testData + "</strong>");
			logger.info(locatorName + " text is set as " + testData);
		} catch (Exception e) {
			logger.info(locatorName + " " + locatorValue
					+ " text is not entered **********");
			report.log(LogStatus.ERROR, locatorName + " " + locatorValue
					+ " text is not set as " + testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			Assert.assertTrue(result, " text is not entered **********");
		}
	}

	public void sleep(String a) {
		ExtentReports report = localReport.get();
		try {
			long l = Long.parseLong(a);
			Thread.sleep(l);
			report.log(LogStatus.INFO, "waiting for " + l + " milliseconds");
			logger.info(a);
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
		}
	}

	public void performClick(By objectname) {
		result = true;
		LocatorSetAndGet locatorObject = extractObject(objectname);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			driver.findElement(locatorValue).click();
			report.log(LogStatus.INFO, locatorName + " is clicked");
			logger.info(locatorName + " is clicked");
		} catch (Exception e) {
			logger.info(locatorName + " " + locatorValue
					+ " is not clicked **********");
			report.log(LogStatus.ERROR, locatorName + " " + locatorValue
					+ " is not clicked");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "Couldn't click");
		}
	}

	public void isjQueryLoaded(WebDriver driver) {
		logger.info("Waiting for ready state complete");
		(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				JavascriptExecutor js = (JavascriptExecutor) d;
				String readyState = js.executeScript(
						"return document.readyState").toString();
				logger.info("Ready State: " + readyState);
				return (Boolean) js
						.executeScript("return !!window.jQuery && window.jQuery.active == 0");
			}
		});
	}

	public String getAttributeValue(By objectName, String attrName) {
		result = true;
		String objectValue = null;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();
		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			objectValue = driver.findElement(locatorValue).getAttribute(
					attrName);
			report.log(LogStatus.INFO, locatorName
					+ " Element attribute value is captured");
			logger.info(locatorName + "Element attribute value is captured");
		} catch (Exception e) {
			report.log(LogStatus.ERROR, locatorName + " " + locatorValue
					+ "Element attribute value is not captured**********");
			logger.info(locatorName + " " + locatorValue
					+ "Element attribute value is not captured**********");
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "Element attribute value is not captured");
		}
		return objectValue;
	}

	public void waitForElementToBeClickable(By objectName) {
		try {
			WebDriverWait wait = localWait.get();
			wait.until(ExpectedConditions.elementToBeClickable(objectName));
		} catch (Exception e) {
			logger.error("Error occured in waitForElementToBeClickable method:"
					+ e);
		}
	}

	public String performGetText(By objectName) {
		result = true;
		String objectValue = null;
		LocatorSetAndGet locatorObject = extractObject(objectName);
		String locatorName = locatorObject.getLocatorName();
		By locatorValue = locatorObject.getLocatoreValue();

		WebDriver driver = localDriver.get();
		ExtentReports report = localReport.get();

		try {
			waitForElementToBeDisplayed(locatorValue);
			pageLoad();
			ajaxWait();
			objectValue = driver.findElement(locatorValue).getText();
			logger.info(locatorName + " text is captured as" + objectValue);
			report.log(LogStatus.INFO, locatorName + " text is captured as "
					+ "<strong>" + objectValue + "</strong>");
		} catch (Exception e) {
			logger.info(locatorName + " " + locatorValue
					+ " could not get the text **********");
			report.log(LogStatus.INFO, locatorName + " " + locatorValue
					+ " could not get the text **********");
			logger.error(e);
			result = false;
			Assert.assertTrue(result, "could not get the text");
		}
		return objectValue;
	}

	public String takeScreenShotExtentReports() {
		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		String timeStamp = t.toString();
		timeStamp = timeStamp.replace(' ', '_');
		timeStamp = timeStamp.replace(':', '_');
		try {
			WebDriver driver = localDriver.get();
			File myImg = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(myImg, new File(getReportLocation()
					+ getReportImageLocation() + timeStamp + ".png"));
		} catch (IOException e) {
			logger.error(e);
		}
		return getReportLocation() + getReportImageLocation() + timeStamp
				+ ".png";
	}

	public String getReportImageLocation() {
		return Constants.CONSTANT_EXTENTREPORT_IMAGES;
	}

	private String getReportLocation() {
		return getProjectPath() + Constants.CONSTANTS_EXTENTREPORT_LOCATION;
	}

}

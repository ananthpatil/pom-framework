package com.sonata.tests.c4c;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.sonata.pages.c4c.HomePage;
import com.sonata.pages.c4c.LoginPage;
import com.sonata.pages.c4c.ReverseFeedPage;
import com.sonata.utility.DataReader;
import com.sonata.utility.DriverCreation;

public final class ReverseFeedTest extends DriverCreation {

	private String browser;
	private String url;
	private String node;

	HomePage homeObject;
	LoginPage loginObject;
	ReverseFeedPage reverseFeedObj;
	DataReader dataReader = new DataReader();

	@Parameters({ "url", "browser", "node" })
	@BeforeMethod
	public void readProperties(String url, String browser, String node) {
		this.browser = browser;
		this.url = url;
		this.node = node;
	}

	@Test
	public void genderVerificationBasedOnTitle() {
		String testCaseName = "TC-12316";
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO:"
					+ testCaseName);
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			reverseFeedObj = new ReverseFeedPage(localDriver, localreport,
					getWaitObject(), logger);
			reverseFeedObj.create_New_Customer(testDataMap.get("Title"),
					testDataMap.get("FirstName"), testDataMap.get("LastName"),
					testDataMap.get("DOB"), testDataMap.get("Email"),
					testDataMap.get("LandLine"), testDataMap.get("SrcMarket"),
					testDataMap.get("Street"), testDataMap.get("Hnum"),
					testDataMap.get("City"), testDataMap.get("PostCode"));
			String c4cId = reverseFeedObj.retrieveCustomerId();
			reverseFeedObj.genderVerification(c4cId, testDataMap.get("Title"));
			reverseFeedObj.tearDownTest();
		}
	}

	@Test
	public void create_Customer_ByEnteringNameAndMobile() {
		String testCaseName = "TC-12336";
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO:"
					+ testCaseName);
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			reverseFeedObj = new ReverseFeedPage(localDriver, localreport,
					getWaitObject(), logger);
			reverseFeedObj.create_New_CustomerForMobile(
					testDataMap.get("Title"), testDataMap.get("FirstName"),
					testDataMap.get("LastName"), testDataMap.get("Email"),
					testDataMap.get("Mobile"), testDataMap.get("SrcMarket"));
			String c4cId = reverseFeedObj.retrieveCustomerId();
			reverseFeedObj.verify_Cust_DataInCdm(c4cId);
			reverseFeedObj.tearDownTest();
		}
	}

	@Test
	public void create_Customer_ByEnteringNameAndLandLine() {
		String testCaseName = "TC-12335";
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO:"
					+ testCaseName);
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			reverseFeedObj = new ReverseFeedPage(localDriver, localreport,
					getWaitObject(), logger);
			reverseFeedObj.create_New_CustomerForLandline(
					testDataMap.get("Title"), testDataMap.get("FirstName"),
					testDataMap.get("LastName"), testDataMap.get("Email"),
					testDataMap.get("LandLine"), testDataMap.get("SrcMarket"));
			String c4cId = reverseFeedObj.retrieveCustomerId();
			reverseFeedObj.verify_Cust_DataInCdm(c4cId);
			reverseFeedObj.tearDownTest();
		}
	}

	@Test
	public void createNewCustomer() {
		String testCaseName = "TC-12313";
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO:"
					+ testCaseName);
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			reverseFeedObj = new ReverseFeedPage(localDriver, localreport,
					getWaitObject(), logger);
			reverseFeedObj.create_New_Customer(testDataMap.get("Title"),
					testDataMap.get("FirstName"), testDataMap.get("LastName"),
					testDataMap.get("DOB"), testDataMap.get("Email"),
					testDataMap.get("LandLine"), testDataMap.get("SrcMarket"),
					testDataMap.get("Street"), testDataMap.get("Hnum"),
					testDataMap.get("City"), testDataMap.get("PostCode"));
			String c4cId = reverseFeedObj.retrieveCustomerId();
			reverseFeedObj.verify_Cust_DataInCdm(c4cId);
			reverseFeedObj.tearDownTest();
		}
	}

	/*
	 * @AfterMethod public void closeBrowser() { loginObject.tearDownTest();
	 * loginObject.stopTest(); }
	 */

	@AfterMethod
	public void closeBrowser() {
		/*
		 * WebDriver driver = localDriver.get(); if (driver != null)
		 * driver.quit(); ExtentReports report = localreport.get(); if (report
		 * != null) report.endTest();
		 */
		homeObject.stopTest();
		homeObject = null;
		loginObject = null;
		reverseFeedObj = null;
	}
}

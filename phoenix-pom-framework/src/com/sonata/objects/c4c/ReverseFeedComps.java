package com.sonata.objects.c4c;

import org.openqa.selenium.By;

public class ReverseFeedComps {
	
	public static By C4C_CreateCustomer_Create = By.xpath("//button[@title='Create']");
	public static By C4C_CreateCustomer_NewCustomer = By.xpath("//section[@class='sapMDialogSection']//span[contains(text(),'Customer')]");
	public static By C4C_CreateCustomer_Title = By.xpath("//span[@title='Title']/../div//div/input");
	public static By C4C_CreateCustomer_TitleSelection = By.xpath("(//b[text()='Mr'])[1]/..");
	public static By C4C_CreateCustomer_FirstName = By.xpath("//span[@title='First Name']/../div//input");
	public static By C4C_CreateCustomer_Lastname = By.xpath("//span[@title='Last Name']/../div//input");
	public static By C4C_CreateCustomer_DOB = By.xpath("//span[@title='Date of Birth']/../div//input");
	public static By C4C_CreateCustomer_Email= By.xpath("//span[@title='E-Mail']/../div//input");
	public static By C4C_CreateCustomer_Mobile= By.xpath("//span[contains(@title,'Mobile')]/../div//input");
	public static By C4C_CreateCustomer_LandLine= By.xpath("//span[@title='Landline']/../div//input");
	public static By C4C_CreateCustomer_SrcMarket= By.xpath("//span[contains(@title,'Source')]/../div//input");
	public static By C4C_CreateCustomer_SrcMarketSel= By.xpath("//b[text()='<UK>']/..");
	public static By C4C_CreateCustomer_Street= By.xpath("//span[contains(@title,'Street')]/../div//input");
	public static By C4C_CreateCustomer_HNum= By.xpath("//span[contains(@title,'House')]/../div//input");
	public static By C4C_CreateCustomer_City= By.xpath("//span[contains(@title,'City')]/../div//input");
	public static By C4C_CreateCustomer_PostCode= By.xpath("//span[contains(@title,'Postal')]/../div//input");
	public static By C4C_CreateCustomer_PrivacyAgreement = By.xpath("//span[contains(@title,'I have informed')]/../div//span[text()='No']");
	public static By C4C_CreateCustomer_Save = By.xpath("(//button[@class='sapMBtn sapMBtnBase'])[1]");
	public static By C4C_CreateCustomer_Open = By.xpath("//div[text()='Item Created']");
	public static By C4C_CreateCustomer_C4CId = By.xpath("//label[contains(text(),'Customer ID')]/../div//span");
	
}

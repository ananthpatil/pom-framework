package com.sonata.pages.c4c;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.sonata.common.TestBase;
import com.sonata.objects.c4c.LoginComps;

public class RecommendationsPage extends TestBase {

	private static final String loginClsName = "com.sonata.objects.c4c.LoginComps";

	ExtentReports report = localReport.get();

	public RecommendationsPage(ThreadLocal<WebDriver> driver,
			ThreadLocal<ExtentReports> report, ThreadLocal<WebDriverWait> wait,
			org.apache.log4j.Logger logger) {
		super(driver, report, wait, logger);
	}

	public void recomendationView(String customerName) {
		setClassName(loginClsName);
		performClick(LoginComps.C4C_Login_CloseHomeIcon);
		performClick(LoginComps.C4C_Login_MainCustomerTab);
		performClick(LoginComps.C4C_Login_SubCustomerTab);
		performEnterText(LoginComps.C4C_Login_SearchCaseTextBox, customerName);
		performEnterKey(LoginComps.C4C_Login_SearchCaseTextBox);
		performClick(LoginComps.C4C_Login_CustomerName);
		performComponentPresent(LoginComps.C4C_Login_CustEmail);
		performComponentPresent(LoginComps.C4C_Login_CustMobile);
		performComponentPresent(LoginComps.C4C_Login_CustAddress);
		performComponentPresent(LoginComps.C4C_Login_CustTitle);
		performComponentPresent(LoginComps.C4C_Login_CustDOB);
		String value = performGetText(LoginComps.C4C_Login_CustNameValue);
		stringComparisonWithContains(customerName, value);
		String c4cId = performGetText(LoginComps.C4C_Login_CustIdVal);
		if (!c4cId.matches("\\d+")) {
			reportFailure();
		}
		performClick(LoginComps.C4C_Login_BookingTab);
		reportPassed();
		tearDownTest();
	}

}

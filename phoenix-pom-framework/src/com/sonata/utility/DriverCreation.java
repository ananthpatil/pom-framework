package com.sonata.utility;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.sonata.constants.Constants;

public class DriverCreation {

	public ThreadLocal<ExtentReports> localreport;
	public DesiredCapabilities cap1;
	private static ThreadLocal<WebDriverWait> localWait = new ThreadLocal<WebDriverWait>();
	public static ThreadLocal<WebDriver> localDriver = new ThreadLocal<WebDriver>();
	protected String testCaseName;
	public Logger logger = Logger.getLogger(this.getClass().toString());
	protected ExcelReader xlsReader;

	/*
	 * private static ThreadLocal<WebDriver> WEBDRIVER = new
	 * ThreadLocal<WebDriver>();
	 * 
	 * public WebDriver getWebDriver(String browser){ WebDriver driver=
	 * WEBDRIVER.get(); if (driver== null) { driver = initDriver(browser);
	 * WEBDRIVER.set(driver); } return driver; }
	 */

	/*
	 * public DriverCreation(String browser,ExtentReports report,Logger logger){
	 * this.browser = browser; this.report = report; this.logger = logger; }
	 */

	/*
	 * public ThreadLocal<ExtentReports> initExtent(String reportName) { //
	 * ExtentReport extent = new ExtentReport(); ThreadLocal<ExtentReports>
	 * localReport = ExtentReport .getExtentReportInstance(reportName);
	 * 
	 * ExcelReader xlsReader = new ExcelReader(getProjectPath() +
	 * Constants.XLS_FILE_PATH); this.xlsReader = xlsReader;
	 * 
	 * this.localreport = localReport; return localreport; }
	 */

	public void initExtent(String reportName) {
		localreport = ExtentReport.getExtentReportInstance(reportName);
		xlsReader = new ExcelReader(getProjectPath() + Constants.XLS_FILE_PATH);
	}

	/*
	 * public void setTestCaseName(String testCaseName) {
	 * logger.info(""+testCaseName); this.testCaseName = testCaseName; }
	 */

	public ThreadLocal<WebDriverWait> getWaitObject() {
		return localWait;
	}

	public void initDriver(String browser, String node) {
		WebDriver driver = null;
		WebDriverWait wait = null;
		Properties prop = PropertiesReader.getProperty();
		ExtentReports report = localreport.get();
		try {
			cap1 = new DesiredCapabilities();
			if (prop.getProperty("grid.execution.mode").equalsIgnoreCase("no")) {
				if (browser.equalsIgnoreCase("Firefox")) {
					System.setProperty("webdriver.gecko.driver",
							Constants.CONSTANTS_GECKO_DRIVER_PATH);
					cap1.setCapability("marionette", true);
					driver = new FirefoxDriver(cap1);
					driver.manage().timeouts()
							.implicitlyWait(2, TimeUnit.SECONDS);
					wait = new WebDriverWait(driver, 50);
					localWait.set(wait);
					localDriver.set(driver);
					report.log(LogStatus.INFO, "Firefox driver has started");
					logger.info("Firefox driver has started");

				} else if (browser.equalsIgnoreCase("Chrome")) {
					System.setProperty(Constants.CONSTANTS_CHROME_PROPERTY,
							Constants.CONSTANTS_CHROME_DRIVER_PATH);
					driver = new ChromeDriver();
					driver.manage().timeouts()
							.implicitlyWait(2, TimeUnit.SECONDS);
					wait = new WebDriverWait(driver, 50);
					localWait.set(wait);
					localDriver.set(driver);
					report.log(LogStatus.INFO, "Chrome driver has started");
					logger.info("Chrome driver has started");
				} else if (browser.equalsIgnoreCase("IE")) {
					System.setProperty(Constants.CONSTANTS_IE_PROPERTY,
							Constants.CONSTANTS_IE_DRIVER_PATH);
					// cap1.internetExplorer();
					cap1.setCapability(
							InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
							true);
					driver = new InternetExplorerDriver(cap1);
					driver.manage().timeouts()
							.implicitlyWait(2, TimeUnit.SECONDS);
					wait = new WebDriverWait(driver, 50);
					localWait.set(wait);
					localDriver.set(driver);
					report.log(LogStatus.INFO,
							"Internet Explorer driver has started");
					logger.info("Internet Explorer driver has started");
				} else if (browser.equalsIgnoreCase("Mobile")) {
					DesiredCapabilities capbility;
					String deviceName = prop.getProperty("device.name");
					System.setProperty(Constants.CONSTANTS_CHROME_PROPERTY,
							Constants.CONSTANTS_CHROME_DRIVER_PATH);

					Map<String, String> mobileEmulation = new HashMap<String, String>();
					mobileEmulation.put("deviceName", deviceName);

					Map<String, Object> chromeOptions = new HashMap<String, Object>();
					chromeOptions.put("mobileEmulation", mobileEmulation);

					capbility = DesiredCapabilities.chrome();
					capbility.setCapability(ChromeOptions.CAPABILITY,
							chromeOptions);
					driver = new ChromeDriver(capbility);
					driver.manage().timeouts()
							.implicitlyWait(2, TimeUnit.SECONDS);
					wait = new WebDriverWait(driver, 50);
					localWait.set(wait);
					localDriver.set(driver);
				}
			} else {
				if (browser.equalsIgnoreCase("Firefox")) {
					System.setProperty("webdriver.gecko.driver",
							Constants.CONSTANTS_GECKO_DRIVER_PATH);
					cap1.setCapability("marionette", true);
					cap1 = DesiredCapabilities.firefox();
					cap1.setBrowserName("firefox");
					cap1.setPlatform(Platform.WINDOWS);
				} else if (browser.equalsIgnoreCase("Chrome")) {
					System.setProperty(Constants.CONSTANTS_CHROME_PROPERTY,
							Constants.CONSTANTS_CHROME_DRIVER_PATH);
					cap1 = DesiredCapabilities.chrome();
					cap1.setBrowserName("chrome");
					cap1.setPlatform(Platform.WINDOWS);
				} else if (browser.equalsIgnoreCase("IE")) {
					System.setProperty(Constants.CONSTANTS_IE_PROPERTY,
							Constants.CONSTANTS_IE_DRIVER_PATH);
					// cap1.internetExplorer();
					cap1 = DesiredCapabilities.internetExplorer();
					cap1.setBrowserName("internet explorer");
					cap1.setPlatform(Platform.WINDOWS);
					cap1.setCapability(
							InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);
					cap1.setCapability(
							InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,
							true);
				}
				driver = new RemoteWebDriver(new URL(node), cap1);
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				wait = new WebDriverWait(driver, 50);
				localWait.set(wait);
				localDriver.set(driver);
				report.log(LogStatus.INFO, browser + " driver has started");
				logger.info(browser + " driver has started");
			}
			// this.wait = localWait.get();
			// driver = localDriver.get();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				Assert.assertTrue(true, "Set up Test");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
		// this.wait = wait;
		//return localDriver;
	}

	private String getProjectPath() {
		File currentDirFile = new File("");
		String path = currentDirFile.getAbsolutePath();
		return path;
	}

}

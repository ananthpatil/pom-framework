package com.sonata.tests.c4c;

import java.util.Map;

import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sonata.pages.c4c.FollowUpComplaintsPage;
import com.sonata.pages.c4c.HomePage;
import com.sonata.pages.c4c.LoginPage;
import com.sonata.utility.DataReader;
import com.sonata.utility.DriverCreation;

public final class FollowUpComplaintsTest extends DriverCreation {
	private String browser;
	private String url;
	private String node;

	HomePage homeObject;
	LoginPage loginObject;
	FollowUpComplaintsPage followUpObject;
	DataReader dataReader = new DataReader();

	@Parameters({ "url", "browser", "node" })
	@BeforeMethod
	public void readProperties(String url, String browser, String node) {
		this.browser = browser;
		this.url = url;
		this.node = node;
	}

	@Test
	public void followUp_Case_For_Complaint() {
		String testCaseName = "TC-11011";
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO:"
					+ testCaseName);
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			followUpObject = new FollowUpComplaintsPage(localDriver,
					localreport, getWaitObject(), logger);
			followUpObject.click_OnCaseTab_SelectOpenItem();
			followUpObject.applyFilter(testDataMap.get("Status"),
					testDataMap.get("ComplaintType"));
			followUpObject.createFollowUpCase();
			followUpObject.tearDownTest();
		}
	}

	@Test
	public void followUp_Case_For_Direct() {
		String testCaseName = "TC-11012";
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO:"
					+ testCaseName);
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			followUpObject = new FollowUpComplaintsPage(localDriver,
					localreport, getWaitObject(), logger);
			followUpObject.click_OnCaseTab_SelectOpenItem();
			followUpObject.applyFilter(testDataMap.get("Status"),
					testDataMap.get("ComplaintType"));
			followUpObject.createFollowUpCase();
			followUpObject.tearDownTest();
		}
	}

	@Test
	public void followUp_Case_For_Sensitive() {
		String testCaseName = "TC-11013";
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO:"
					+ testCaseName);
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			followUpObject = new FollowUpComplaintsPage(localDriver,
					localreport, getWaitObject(), logger);
			followUpObject.click_OnCaseTab_SelectOpenItem();
			followUpObject.applyFilter(testDataMap.get("Status"),
					testDataMap.get("ComplaintType"));
			followUpObject.createFollowUpCase();
			followUpObject.tearDownTest();
		}
	}

	@AfterMethod
	public void closeBrowser() {
		homeObject.stopTest();
		homeObject = null;
		loginObject = null;
		followUpObject = null;
	}

}

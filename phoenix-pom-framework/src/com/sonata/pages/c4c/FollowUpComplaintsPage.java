package com.sonata.pages.c4c;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.sonata.common.TestBase;
import com.sonata.objects.c4c.CaseComps;
import com.sonata.objects.c4c.LoginComps;

public class FollowUpComplaintsPage extends TestBase {
	private static final String loginClsName = "com.sonata.objects.c4c.LoginComps";
	private static final String caseClsname = "com.sonata.objects.c4c.CaseComps";

	public FollowUpComplaintsPage(ThreadLocal<WebDriver> driver,
			ThreadLocal<ExtentReports> report, ThreadLocal<WebDriverWait> wait,
			org.apache.log4j.Logger logger) {
		super(driver, report, wait, logger);
	}

	/**
	 * <p>
	 * The method to click on Main case and sub case tab then Select open Item
	 * option
	 */
	public void click_OnCaseTab_SelectOpenItem() {
		setClassName(loginClsName);
		performClick(LoginComps.C4C_Login_MainCaseTab);
		performClick(LoginComps.C4C_Login_SubCaseTab);
		setClassName(caseClsname);
		performClick(CaseComps.C4C_Case_ClickOnDropDownArrow);
		performClick(CaseComps.C4C_Case_AllOpenItem);
	}

	public void applyFilter(String status, String complaintType) {
		setClassName(caseClsname);
		performClick(CaseComps.C4C_Case_Status);
		performEnterText(CaseComps.C4C_Case_Filter, status);
		performEnterKey(CaseComps.C4C_Case_Filter);
		performClick(CaseComps.C4C_Case_CaseType);
		performEnterText(CaseComps.C4C_Case_Filter, complaintType);
		performEnterKey(CaseComps.C4C_Case_Filter);
	}

	public void createFollowUpCase() {
		boolean flag = isElementPresent(CaseComps.C4C_Case_CaseId);
		if (flag) {
			performClick(CaseComps.C4C_Case_CaseId);
			performClick(CaseComps.C4C_Case_ForwardIcon);
			performClick(CaseComps.C4C_Case_RelatedItemTab);
			performClick(CaseComps.C4C_Case_More);
			performClick(CaseComps.C4C_Case_CreateCase);
			performClick(CaseComps.C4C_Case_SaveAndOpen);
			String subCaseId = performGetText(CaseComps.C4C_Case_SubCaseId);
			if (subCaseId != null) {
				subCaseId = subCaseId.replaceAll("[a-zA-Z\\s\\-]*", "").trim();
				if (subCaseId.matches("\\d+")) {
					reportPassed();
				} else {
					reportFailure();
				}
			}
		} else {
			reportNoData();
		}
	}

}

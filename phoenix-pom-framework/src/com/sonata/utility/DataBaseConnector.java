package com.sonata.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;

public class DataBaseConnector {

	private String driverName;
	private String dbUrl;
	private String dbUserName;
	private String dbPassword;
	private String dbName;
	private Logger logger1 = Logger.getLogger(DataBaseConnector.class);

	Connection connection;
	Statement statement;
	ResultSet resultSet;

	public DataBaseConnector() {
		Properties prop = PropertiesReader.getProperty();
		dbUserName = prop.getProperty("db.user.name");
		dbPassword = prop.getProperty("db.user.password");
		dbUrl = prop.getProperty("db.url");
		dbName = prop.getProperty("db.name");
		driverName = prop.getProperty("db.driver.name");
	}

	private void createConnection() {
		try {
			Class.forName(driverName);
			connection = DriverManager.getConnection(dbUrl + ":" + dbName,
					dbUserName, dbPassword);
			logger1.info("DB connection established successfully");
		} catch (ClassNotFoundException e) {
			logger1.error(e);
		} catch (SQLException e) {
			logger1.error(e);
		} catch (Exception e) {
			logger1.info("DB connection not established");
		}
	}

	public synchronized ResultSet executeQuery(String sqlquery) {
		createConnection();
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlquery);
			/*
			 * while (resultSet.next()) { logger1.info(resultSet.getInt(1) +
			 * "  " + resultSet.getString(2) + "  " + resultSet.getString(3)); }
			 */
			logger1.info("Resulset set object created successfully");
		} catch (SQLException e) {
			logger1.error(e);
		} catch (Exception e) {
			logger1.error(e);
		}
		return resultSet;
	}

	public synchronized void disconnect() {
		try {
			if (statement != null)
				statement.close();
			if (resultSet != null)
				resultSet.close();
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			logger1.error(e);
		}
	}
}

package com.sonata.tests.c4c;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.sonata.pages.c4c.HomePage;
import com.sonata.pages.c4c.LoginPage;
import com.sonata.pages.c4c.RecommendationsPage;
import com.sonata.utility.DataReader;
import com.sonata.utility.DriverCreation;

public final class RecommendationsTest extends DriverCreation {
	private String browser;
	private String url;
	private String node;

	HomePage homeObject;
	LoginPage loginObject;
	RecommendationsPage recommendObject;
	WebDriver driver1;
	DataReader dataReader = new DataReader();

	@Parameters({ "url", "browser", "node" })
	@BeforeTest
	public void readProperties(String url, String browser, String node) {

		this.browser = browser;
		this.url = url;
		this.node = node;
	}

	@Test
	public void createComplaintCaseWithDefault() {
		String testCaseName = "TC-11037";
		// ThreadLocal<ExtentReports> report = initExtent(testCaseName);
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO");
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			// ThreadLocal<WebDriver> driver = initDriver(browser, node);
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			recommendObject = new RecommendationsPage(localDriver, localreport,
					getWaitObject(), logger);
			recommendObject.recomendationView(testDataMap.get("CustomerName"));
			recommendObject.tearDownTest();
			recommendObject.stopTest();
		}
	}

	/*
	 * @AfterMethod public void closeBrowser() { loginObject.tearDownTest();
	 * loginObject.stopTest(); }
	 */

	@AfterMethod
	public void closeBrowser() {
		WebDriver driver = localDriver.get();
		if (driver != null)
			driver.quit();
		ExtentReports report = localreport.get();
		if (report != null)
			report.endTest();
		homeObject = null;
		loginObject = null;
		recommendObject = null;
	}
}

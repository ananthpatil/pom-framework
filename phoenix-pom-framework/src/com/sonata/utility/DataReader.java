package com.sonata.utility;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import com.sonata.constants.Constants;

public class DataReader {

	public boolean isTestCaseExecute(ExcelReader xlsReader, String testCase) {
		String sheetName = Constants.XLS_FILE_TESTCASESHEET;
		int totRows = xlsReader.getRowCount(sheetName);
		for (int r = 2; r <= totRows; r++) {
			String testCaseName = xlsReader.getCellData(sheetName,
					"TestCaseName", r);
			if (testCaseName.equalsIgnoreCase(testCase)) {
				String runMode = xlsReader.getCellData(sheetName, "RunMode", r);
				if (runMode.equalsIgnoreCase("yes"))
					return true;
				else
					return false;
			}
		}
		return false;
	}

	public synchronized Object[][] getTestData(ExcelReader xlsReader,
			String testCase) {
		String sheetName = Constants.XLS_FILE_TESTDATASHEET;
		int tstStartRow = 1, tstHeaderRow, tstDataRow;
		int totalRows = xlsReader.getRowCount(sheetName);
		for (int r = 1; r <= totalRows; r++) {
			String testCaseName = xlsReader.getCellData(sheetName, 0, r);
			if (testCase.equalsIgnoreCase(testCaseName)) {
				tstStartRow = r;
				break;
			}
		}
		tstHeaderRow = tstStartRow + 1;
		tstDataRow = tstStartRow + 2;
		int rows = 0;
		while (true) {
			if (xlsReader.getCellData(sheetName, 1, tstDataRow + rows).equals(
					""))
				break;
			rows++;
		}

		int cols = 1;
		while (true) {
			if (xlsReader.getCellData(sheetName, cols, tstHeaderRow).equals(""))
				break;
			cols++;
		}
		cols = cols - 1;
		Object[][] dataObject = new Object[rows][1];
		Map<String, String> dataMap;
		int totTd = 0;
		for (int dataRow = tstDataRow; dataRow < (tstDataRow + rows); dataRow++) {
			dataMap = new ConcurrentHashMap<String, String>();
			for (int c = 1; c <= cols; c++) {
				String labelName = xlsReader.getCellData(sheetName, c,
						tstHeaderRow).trim();
				String testdata = xlsReader.getCellData(sheetName, c, dataRow)
						.trim();
				dataMap.put(labelName, testdata);
			}
			dataObject[totTd][0] = dataMap;
			totTd++;
		}
		return dataObject;
	}

}

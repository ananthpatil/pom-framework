package com.sonata.pages.c4c;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.sonata.common.TestBase;
import com.sonata.objects.c4c.HomeComps;
import com.sonata.objects.c4c.LoginComps;

public class LoginPage extends TestBase {

	private static final String loginClassName = "com.sonata.objects.c4c.LoginComps";
	private static final String homeClassName = "com.sonata.objects.c4c.HomeComps";

	/**
	 * <p>
	 * Constructor to pass the value to super class
	 * </p>
	 * 
	 * @param driver
	 * @param report
	 * @param wait
	 * @param logger
	 */
	public LoginPage(ThreadLocal<WebDriver> driver,
			ThreadLocal<ExtentReports> report, ThreadLocal<WebDriverWait> wait,
			org.apache.log4j.Logger logger) {
		super(driver, report, wait, logger);
	}

	/**
	 * <p>
	 * The method to create the Case
	 * </p>
	 */

	public void closeBrowser() {

	}

	public void login(String userName, String password) {
		ajaxWait();
		setClassName(homeClassName);
		performEnterText(HomeComps.C4C_Home_UesrID, userName);
		performEnterText(HomeComps.C4C_Home_Password, password);
		performClick(HomeComps.C4C_Home_Submit);
		//setClassName(loginClassName);
		//performClick(LoginComps.C4C_Login_CloseHomeIcon);
	}

	public void logOut() {
		setClassName(loginClassName);
		performClick(LoginComps.C4C_Login_MainLogout);
		performClick(LoginComps.C4C_Login_LogoutYes);
	}
}

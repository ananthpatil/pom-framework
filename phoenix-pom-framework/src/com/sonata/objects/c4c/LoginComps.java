package com.sonata.objects.c4c;

import org.openqa.selenium.By;

public class LoginComps {
	
	public static By C4C_Login_CloseHomeIcon = By.xpath("//div[@class='comSapClientCodCoreHomePageControl-left-toolbar sapUiVlt sapuiVlt']");
	public static By C4C_Login_MainCaseTab1 = By.xpath("(//h1[contains(text(),'Cases')])[1]");
	public static By C4C_Login_MainCaseTab = By.xpath("(//a[contains(text(),'Cases')])[1]");
	public static By C4C_Login_SubCaseTab1 = By.xpath("//a[contains(text(),'Cases')]");
	public static By C4C_Login_SubCaseTab = By.xpath("(//a[contains(text(),'Cases')])[2]");//pat
	public static By C4C_Login_CreateCase1 = By.xpath("//button[@title='Create']");
	public static By C4C_Login_CreateCase = By.xpath("//button[text()='Create Case']");//pat
	public static By C4C_Login_CustomerNameIconClick = By.xpath("//button[contains(@id,'objectvalueselectorCNRITQC')]");
	public static By C4C_Login_ClickHere = By.xpath("//a[contains(text(),'Click here')]");
	public static By C4C_Login_ComplaintTypeArrow = By.xpath("//label[contains(text(),'Case Type')]/../div//div[@class='sapUiTfComboIcon']");
	public static By C4C_Login_ComplaintType = By.xpath("//ul[contains(@id,'dropdownlis')]/li");
	public static By C4C_Login_SearchCustomer = By.xpath("(//input[@type='search'])[1]");
	public static By C4C_Login_ClickCustomer = By.xpath("//div[@class='sapUiDlgCont']//div[@class='sapUiTableCCnt']//tr[1]/td[1]");
	public static By C4C_Login_ComplDescription = By.xpath("//label[contains(text(),'Description:')]/..//textarea");
	public static By C4C_Login_ComplCategoryArrow = By.xpath("//label[contains(text(),'Complaint Category')]/../div//div[@class='sapUiTfComboIcon']");
	public static By C4C_Login_ComplaintCategory = By.xpath("(//div[@id='sap-ui-static']//div[contains(@class,'sapUiLbx ')])[2]//ul/li");
	public static By C4C_Login_ComplSaveAndOpen = By.xpath("//button[text()='Save and Open']");
	
	public static By C4C_Login_SelectAllArrow = By.xpath("(//div[@class='sapUiTbCont'])[2]//div[@class='sapUiTfComboIcon']");
	public static By C4C_Login_SelectAllCase = By.xpath("(//ul[contains(@id,'DefaultSetDropDown')])[1]/li/span");
	public static By C4C_Login_SearchCaseTextBox = By.xpath("(//div[@class='sapUiTbCont'])[3]//input");
	public static By C4C_Login_ComplaintID = By.xpath("//div[@class='sapUiTableCell']//a[@title='1234']");
	public static By C4C_Login_Edit = By.xpath("//button[text()='Edit']");
	public static By C4C_Login_StatusArrow = By.xpath("//label[text()='Status:']/..//div[@class='sapUiTfComboIcon']");
	public static By C4C_Login_SelectReopen=By.xpath("//div[contains(@class,'sapUiUx3Overlay')]/following-sibling::div[contains(@class,'sapUiLbx')]");
	public static By C4C_Login_ReopenReasonArrow = By.xpath("//label[text()='Reopen Reason:']/..//div[@class='sapUiTfComboIcon']");
	public static By C4C_Login_ReopenReasonText = By.xpath("(//div[contains(@class,'sapUiUx3Overlay')]/following-sibling::div[contains(@class,'sapUiLbx')])[2]//ul//li[position()>1]");
	
	public static By C4C_Login_MainCustomerTab = By.xpath("(//a[contains(text(),'Customers')])[1]");
	public static By C4C_Login_SubCustomerTab = By.xpath("(//a[contains(text(),'Customers')])[2]");
	public static By C4C_Login_CustomerName = By.xpath("//div[@class='sapUiTableCell']//a[@title='Perfrtqrrerssr Perfyarartpuru']");
	public static By C4C_Login_CustEmail = By.xpath("(//label[contains(text(),'E-Mail')])[1]");
	public static By C4C_Login_CustMobile = By.xpath("(//label[contains(text(),'Mobile')])[1]");
	public static By C4C_Login_CustAddress = By.xpath("(//label[contains(text(),'Address')])[1]");
	public static By C4C_Login_CustNameValue = By.xpath("(//label[contains(text(),'Name')])[1]/following-sibling::div//span");
	public static By C4C_Login_CustTitle = By.xpath("(//label[contains(text(),'Title')])[1]");
	public static By C4C_Login_CustIdVal = By.xpath("//label[contains(text(),'Customer ID')]/following::div[1]//span");
	public static By C4C_Login_CustDOB = By.xpath("(//label[contains(text(),'Date of Birth')])[1]");
	public static By C4C_Login_BookingTab = By.xpath("//a[text()='BOOKINGS']");

	public static By C4C_Login_MainLogout = By.xpath("//a[contains(@class,'logout')]");
	public static By C4C_Login_LogoutYes = By.xpath("//button[text()='Yes']");
	
	
	
	


}

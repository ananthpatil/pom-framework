package com.sonata.pages.c4c;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.sonata.common.TestBase;

public class HomePage extends TestBase {

	public HomePage(ThreadLocal<WebDriver> driver,
			ThreadLocal<ExtentReports> report, ThreadLocal<WebDriverWait> wait,
			org.apache.log4j.Logger logger) {
		super(driver, report, wait, logger);
	}

	public void goToHomepge(String url) {
		launchApplication(url);
	}

}

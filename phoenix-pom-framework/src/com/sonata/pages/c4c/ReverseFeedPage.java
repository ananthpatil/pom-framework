package com.sonata.pages.c4c;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.sonata.common.TestBase;
import com.sonata.objects.c4c.ReverseFeedComps;
import com.sonata.utility.DataBaseConnector;

public class ReverseFeedPage extends TestBase {

	private static final String revreseFeedClsName = "com.sonata.objects.c4c.ReverseFeedComps";

	public ReverseFeedPage(ThreadLocal<WebDriver> driver,
			ThreadLocal<ExtentReports> report, ThreadLocal<WebDriverWait> wait,
			org.apache.log4j.Logger logger) {
		super(driver, report, wait, logger);
	}

	public void create_New_Customer(String title, String fName, String lName,
			String dob, String email, String lanLine, String srcMarket,
			String street, String Hnum, String city, String pstCode) {
		setClassName(revreseFeedClsName);
		performClick(ReverseFeedComps.C4C_CreateCustomer_Create);
		performClick(ReverseFeedComps.C4C_CreateCustomer_NewCustomer);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Title, title);
		performClick(ReverseFeedComps.C4C_CreateCustomer_TitleSelection);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_FirstName, fName);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Lastname, lName);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_DOB, dob);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Email, email);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_LandLine, lanLine);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_SrcMarket,
				srcMarket);
		performClick(ReverseFeedComps.C4C_CreateCustomer_SrcMarketSel);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Street, street);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_HNum, Hnum);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_City, city);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_PostCode, pstCode);
		performClick(ReverseFeedComps.C4C_CreateCustomer_PrivacyAgreement);
		performClick(ReverseFeedComps.C4C_CreateCustomer_Save);
	}

	public void create_New_CustomerForMobile(String title, String fName,
			String lName, String email, String mobile, String srcMarket) {
		setClassName(revreseFeedClsName);
		performClick(ReverseFeedComps.C4C_CreateCustomer_Create);
		performClick(ReverseFeedComps.C4C_CreateCustomer_NewCustomer);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Title, title);
		performClick(ReverseFeedComps.C4C_CreateCustomer_TitleSelection);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_FirstName, fName);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Lastname, lName);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Email, email);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Mobile, mobile);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_SrcMarket,
				srcMarket);
		performClick(ReverseFeedComps.C4C_CreateCustomer_SrcMarketSel);
		performClick(ReverseFeedComps.C4C_CreateCustomer_PrivacyAgreement);
		performClick(ReverseFeedComps.C4C_CreateCustomer_Save);
	}

	public void create_New_CustomerForLandline(String title, String fName,
			String lName, String email, String landline, String srcMarket) {
		setClassName(revreseFeedClsName);
		performClick(ReverseFeedComps.C4C_CreateCustomer_Create);
		performClick(ReverseFeedComps.C4C_CreateCustomer_NewCustomer);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Title, title);
		performClick(ReverseFeedComps.C4C_CreateCustomer_TitleSelection);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_FirstName, fName);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Lastname, lName);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_Email, email);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_LandLine, landline);
		performEnterText(ReverseFeedComps.C4C_CreateCustomer_SrcMarket,
				srcMarket);
		performClick(ReverseFeedComps.C4C_CreateCustomer_SrcMarketSel);
		performClick(ReverseFeedComps.C4C_CreateCustomer_PrivacyAgreement);
		performClick(ReverseFeedComps.C4C_CreateCustomer_Save);
	}

	public String retrieveCustomerId() {
		performClick(ReverseFeedComps.C4C_CreateCustomer_Open);
		String c4cId = performGetText(ReverseFeedComps.C4C_CreateCustomer_C4CId);
		return c4cId;
	}

	public void verify_Cust_DataInCdm(String c4cId) {
		DataBaseConnector dbObject = new DataBaseConnector();
		ResultSet rsltSet = dbObject
				.executeQuery("Select * From Customer_Foreign_Key_Link where X_Customer_SRC_SYS_ID='"
						+ c4cId + "'");
		try {
			if (rsltSet.isBeforeFirst()) {
				while (rsltSet.next()) {
					String cdmIdvalue = rsltSet.getString("X_CUSTOMER_ID");
					localReport.get().log(LogStatus.INFO,
							"CDM id captured as:" + cdmIdvalue);
					if (cdmIdvalue.matches("\\d+")) {
						reportPassed();
					} else {
						reportFailure();
					}
					break;
				}
			} else {
				localReport
						.get()
						.log(LogStatus.INFO,
								"CDM id captured as empty value hence no data written to cdm");
				reportFailure();
			}
		} catch (SQLException e) {
			reportFailure();
		}
		dbObject.disconnect();
	}

	public void genderVerification(String c4cId, String title) {
		DataBaseConnector dbObject = new DataBaseConnector();
		ResultSet rsltSet = dbObject
				.executeQuery("Select * From Customer_Foreign_Key_Link where X_Customer_SRC_SYS_ID='"
						+ c4cId + "'");
		try {
			if (rsltSet.isBeforeFirst()) {
				while (rsltSet.next()) {
					String cdmIdValue = rsltSet.getString("X_CUSTOMER_ID");
					logger.info("CDM id captured as:" + cdmIdValue);
					localReport.get().log(LogStatus.INFO,
							"CDM id captured as:" + cdmIdValue);
					ResultSet rsltSet1 = dbObject
							.executeQuery("select * from customer where X_Customer_Id='"
									+ cdmIdValue + "'");
					while (rsltSet1.next()) {
						String gender = rsltSet1.getString("X_GENDER");
						logger.info("Gender captured from DB as:" + gender);
						localReport.get().log(LogStatus.INFO,
								"Gender captured from DB as:" + gender);
						title = title.toLowerCase();
						switch (title) {
						case "mr":
							if (gender.equals("F"))
								reportFailure();
							break;
						case "mrs":
							if (gender.equals("M"))
								reportFailure();
							break;
						default:
							reportPassed();
						}
					}
					break;
				}
			} else {
				localReport
						.get()
						.log(LogStatus.INFO,
								"CDM id captured as empty value hence no data written to cdm");
				reportFailure();
			}
		} catch (SQLException e) {
			reportFailure();
		}
		dbObject.disconnect();
	}

}

package com.sonata.utility;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.sonata.constants.Constants;

public class ExtentReport {

	/*
	 * ThreadLocal<ExtentReports> localReportObj = new
	 * ThreadLocal<ExtentReports>() {
	 * 
	 * @Override protected ExtentReports initialValue() { return new
	 * ExtentReports(); } };
	 */
	private static ThreadLocal<ExtentReports> localReportObj = new ThreadLocal<ExtentReports>();

	public static ThreadLocal<ExtentReports> getExtentReportInstance(
			String reportName) {
		//ExtentReports report = localReportObj.get();
		//if (report == null) {
			ExtentReports report = new ExtentReports();
			localReportObj.set(report);
			report = localReportObj.get();
			report.init(getReportLocation() + reportName + ".html", true);
			report.config().documentTitle(reportName + " Report");
			report.config().reportHeadline(
					reportName + "execution details using ExtentReports");
			report.config().displayCallerClass(false);
			report.config().useExtentFooter(false);
			report.config().statusIcon(LogStatus.PASS, "check-circle");
			report.startTest(reportName);
			localReportObj.set(report);
		//}
		return localReportObj;
	}

	private static String getReportLocation() {
		return System.getProperty("user.dir")
				+ Constants.CONSTANTS_EXTENTREPORT_LOCATION;
	}

}

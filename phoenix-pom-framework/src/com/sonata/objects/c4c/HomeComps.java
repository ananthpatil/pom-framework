package com.sonata.objects.c4c;

import org.openqa.selenium.By;

public class HomeComps {
	public static By C4C_Home_UesrID = By.xpath("//input[@placeholder='User ID']");
	public static By C4C_Home_Password = By.xpath("//input[@placeholder='Password']");
	public static By C4C_Home_Submit = By.xpath("//span[contains(text(),'Log On')]");
	
}

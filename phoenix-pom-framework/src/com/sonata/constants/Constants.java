package com.sonata.constants;

public class Constants {

	// Driver Path Details
	public static final String CONSTANTS_CHROME_DRIVER_PATH = "Jars/chromedriver.exe";
	public static final String CONSTANTS_IE_DRIVER_PATH = "Jars/IEDriverServer32Bit-1.0.exe";
	public static final String CONSTANTS_PHANTOM_DRIVER_PATH="Jars/IEDriverServer32Bit-1.0.exe";
	public static final String CONSTANTS_GECKO_DRIVER_PATH="Jars/geckodriver.exe";

	// Driver Property Details
	public static final String CONSTANTS_CHROME_PROPERTY = "webdriver.chrome.driver";
	public static final String CONSTANTS_IE_PROPERTY = "webdriver.ie.driver";
	public static final String CONSTANTS_PHANTOM_PROPERTY = "phantomjs.binary.path";

	// Reports and Screenshot Details for Extent Reporting
	public static final String CONSTANTS_EXTENTREPORT_LOCATION = "/reports/";
	public static final String CONSTANT_EXTENTREPORT_IMAGES = "/images/";
	
	//XLS path
	public static final String XLS_FILE_PATH = "/TestData/Data.xls/";
	public static final String XLS_FILE_TESTCASESHEET = "TestCase";
	public static final String XLS_FILE_TESTDATASHEET = "TestData";

	//Properties Path
	public static final String POM_PROPERTY_PATH = "/resources/pom.properties";
	
}

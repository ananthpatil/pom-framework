package com.sonata.objects.c4c;

import org.openqa.selenium.By;

public class CaseComps
{
   public static By C4C_Case_CaseID = By.xpath("(//div[@class='sapUiUx3TVTitle']/span)[1]");
   public static By C4C_Case_ClickOnDropDownArrow = By.xpath("(//span[contains(text(),'Unassigned')]/../../div)[4]");
   public static By C4C_Case_AllOpenItem = By.xpath("//li[text()='All Open Team Cases']");
   public static By C4C_Case_Status = By.xpath("(//bdi[text()='Status']/../../../..)[1]");
   public static By C4C_Case_Filter = By.xpath("//label[text()='Filter']/../div/input");
   public static By C4C_Case_CaseType = By.xpath("//th[@title='Case Type']");
   public static By C4C_Case_CaseId = By.xpath("(//div[@class='sapMScrollContScroll']//table[2]/tbody/tr/td[3]//a)[1]");
   public static By C4C_Case_ForwardIcon = By.xpath("//button[@title='Scroll Forward']");
   public static By C4C_Case_RelatedItemTab = By.xpath("//span[text()='RELATED ITEMS']/..");
   public static By C4C_Case_More = By.xpath("(//bdi[text()='More']/..)[1]");
   public static By C4C_Case_CreateCase = By.xpath("//bdi[text()='Create Case']/..");
   public static By C4C_Case_SaveAndOpen = By.xpath("//span[text()='Save and Open']/..");
   public static By C4C_Case_SubCaseId = By.xpath("//div[@class='sapClientMODHObjNameWrap']//span");
   
   public static By C4C_Case_AllCases = By.xpath("//li[text()='All Cases']");
   public static By C4C_Case_Edit = By.xpath("//button[@title='Edit']");
   public static By C4C_Case_StatusArrow = By.xpath("//bdi[text()='Status']/../..//span[contains(@class,'sapMComboBoxArrow')]");
   public static By C4C_Case_PendingOption = By.xpath("//li[text()='Pending']");
 
 
 
}

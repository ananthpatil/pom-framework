package com.sonata.tests.c4c;

import java.util.Map;

import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sonata.pages.c4c.ComplaintPage;
import com.sonata.pages.c4c.HomePage;
import com.sonata.pages.c4c.LoginPage;
import com.sonata.utility.DataReader;
import com.sonata.utility.DriverCreation;

public final class ComplainTest extends DriverCreation {

	private String browser;
	private String url;
	private String node;

	HomePage homeObject;
	LoginPage loginObject;
	ComplaintPage compObject;
	DataReader dataReader = new DataReader();

	@Parameters({ "url", "browser", "node" })
	@BeforeMethod
	public void readProperties(String url, String browser, String node) {
		this.browser = browser;
		this.url = url;
		this.node = node;
	}

	@Test
	public void createComplaintCaseWithDefault() {
		String testCaseName = "TC-10672";
		// ThreadLocal<ExtentReports> report = initExtent(testCaseName);
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO");
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			// ThreadLocal<WebDriver> driver = initDriver(browser, node);
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			compObject = new ComplaintPage(localDriver, localreport,
					getWaitObject(), logger);
			compObject.createCase(testDataMap.get("ComplaintType"),
					testDataMap.get("CustomerName"),
					testDataMap.get("ComplaintCategory"));
			compObject.complaintPageVerification();
			compObject.tearDownTest();
		}
	}

	@Test
	public void createComplaintCaseWithSensitive() {
		String testCaseName = "TC-10692";
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO");
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];

			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			compObject = new ComplaintPage(localDriver, localreport,
					getWaitObject(), logger);
			compObject.createCase(testDataMap.get("ComplaintType"),
					testDataMap.get("CustomerName"),
					testDataMap.get("ComplaintCategory"));
			compObject.complaintPageVerification();
			compObject.tearDownTest();
		}
	}

	@Test
	public void reopenComplaintFieldVerfication() {
		String testCaseName = "TC-10958";
		initExtent(testCaseName);

		if (!dataReader.isTestCaseExecute(xlsReader, testCaseName)) {
			logger.info("Test Case not executed as its run mode is NO");
			throw new SkipException("");
		}

		Object[][] testData = dataReader.getTestData(xlsReader, testCaseName);
		for (int t = 0; t < testData.length; t++) {
			initDriver(browser, node);
			homeObject = new HomePage(localDriver, localreport,
					getWaitObject(), logger);
			homeObject.goToHomepge(url);

			@SuppressWarnings("unchecked")
			Map<String, String> testDataMap = (Map<String, String>) testData[t][0];
			loginObject = new LoginPage(localDriver, localreport,
					getWaitObject(), logger);
			loginObject.login(testDataMap.get("UserName"),
					testDataMap.get("Password"));

			compObject = new ComplaintPage(localDriver, localreport,
					getWaitObject(), logger);
			compObject.reopenReasonFieldVerification(testDataMap
					.get("ComplaintID"));
			compObject.tearDownTest();
		}
	}

	/*
	 * @AfterMethod public void closeBrowser() { loginObject.tearDownTest();
	 * loginObject.stopTest(); }
	 */

	@AfterMethod
	public void closeBrowser() {
		/*
		 * if (localDriver.get() != null) localDriver.get().quit(); if
		 * (localreport.get() != null) localreport.get().endTest();
		 */
		homeObject.stopTest();
		homeObject.tearDownTest();
		homeObject = null;
		loginObject = null;
		compObject = null;
	}
}

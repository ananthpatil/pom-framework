package com.sonata.utility;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.relevantcodes.extentreports.ExtentReports;

public class ObjectReader {

	Logger logger;
	ExtentReports report;

	public ObjectReader(Logger logger, ExtentReports report) {
		this.logger = logger;
		this.report = report;
	}

	@SuppressWarnings("unused")
	public ConcurrentHashMap<String, By> storeObjects(
			String packageNameWithclassName) {
		Class<?> cls;
		ConcurrentHashMap<String, By> objectMap = new ConcurrentHashMap<String, By>();
		try {
			cls = Class.forName(packageNameWithclassName);
			Object obj = cls.newInstance();
			Field[] fields = cls.getDeclaredFields();
			for (int f = 0; f < fields.length; f++) {
				Class<?> cls1 = fields[f].getType();
				objectMap.put(fields[f].getName(), (By) fields[f].get(obj));
			}
		} catch (InstantiationException e) {
			logger.error("Unable to created the instance for "
					+ packageNameWithclassName);
		} catch (IllegalAccessException e) {
			logger.error("IllegalAccessException exception "
					+ packageNameWithclassName);
		} catch (ClassNotFoundException e) {
			logger.error("Given class couldn't found "
					+ packageNameWithclassName);
		}
		return objectMap;
	}

}

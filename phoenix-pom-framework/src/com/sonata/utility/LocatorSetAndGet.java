package com.sonata.utility;

import org.openqa.selenium.By;

public class LocatorSetAndGet {

	private String objectName;
	private By locatorValue;

	public void setLocatorName(String objectName) {
		this.objectName = objectName;
	}

	public String getLocatorName() {
		return objectName;
	}

	public void setLocatoreValue(By locatorValue) {
		this.locatorValue = locatorValue;
	}

	public By getLocatoreValue() {
		return locatorValue;
	}

}

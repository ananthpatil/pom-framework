package com.sonata.utility;

import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;

public class ExcelReader {

	private FileInputStream fis;
	private static ThreadLocal<HSSFWorkbook> workbook1 = new ThreadLocal<HSSFWorkbook>();
	// private HSSFSheet sheet;
	// private HSSFRow row;
	// private HSSFCell cell;
	private String excelPath;

	@SuppressWarnings("unused")
	public ExcelReader(String excelPath) {
		this.excelPath = excelPath;
		HSSFWorkbook workbook = workbook1.get();
		try {
			fis = new FileInputStream(excelPath);
			workbook = new HSSFWorkbook(fis);
			workbook1.set(workbook);
			HSSFSheet sheet = workbook1.get().getSheetAt(0);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getRowCount(String sheetName) {
		int index = workbook1.get().getSheetIndex(sheetName);
		if (index == -1)
			return 0;
		else {
			HSSFSheet sheet = workbook1.get().getSheetAt(index);
			int number = sheet.getLastRowNum() + 1;
			return number;
		}
	}

	public int getColumnCount(String sheetName, int rowNum) {
		boolean exist = sheetExist(sheetName);
		if (exist == false)
			return -1;
		HSSFSheet sheet = workbook1.get().getSheet(sheetName);
		HSSFRow row = sheet.getRow(rowNum);
		if (row == null)
			return -1;
		else {
			return row.getLastCellNum();
		}
	}

	private boolean sheetExist(String sheetName) {
		int index = workbook1.get().getSheetIndex(sheetName);
		if (index == -1) {
			index = workbook1.get().getSheetIndex(sheetName.toUpperCase());
			if (index == -1)
				return false;
			else
				return true;
		} else {
			return true;
		}
	}

	@SuppressWarnings("unused")
	public boolean createSheet(String sheetName) {
		try {
			HSSFSheet sheet = workbook1.get().createSheet(sheetName);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean removeSheet(String sheetName) {
		if (sheetExist(sheetName)) {
			try {
				int index = workbook1.get().getSheetIndex(sheetName);
				workbook1.get().removeSheetAt(index);
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	public String getCellData(String sheetName, String colHeader, int rowNum) {
		int colNum = -1;
		try {
			if (rowNum <= 0)
				return "";
			if (!sheetExist(sheetName))
				return "";
			int index = workbook1.get().getSheetIndex(sheetName);
			HSSFSheet sheet = workbook1.get().getSheetAt(index);

			HSSFRow row = sheet.getRow(0);
			for (int col = 0; col < row.getLastCellNum(); col++) {
				if (row.getCell(col).getStringCellValue().replace(" ", "")
						.equalsIgnoreCase(colHeader)) {
					colNum = col;
					break;
				}
			}
			if (colNum == -1)
				return "";

			row = sheet.getRow(rowNum - 1);
			if (row == null)
				return "";
			HSSFCell cell = row.getCell(colNum);
			if (cell == null)
				return "";
			else if (cell.getCellType() == CellType.STRING)
				return cell.getStringCellValue();
			else if (cell.getCellType() == CellType.BLANK)
				return "";
			else if (cell.getCellType() == CellType.BOOLEAN)
				return String.valueOf(cell.getBooleanCellValue());
			else
				return String.valueOf(cell.getNumericCellValue());

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public String getCellData(String sheetName, int colNum, int rowNum) {
		try {
			if (rowNum <= 0)
				return "";
			int index = workbook1.get().getSheetIndex(sheetName);
			if (index == -1)
				return "";
			HSSFSheet sheet = workbook1.get().getSheetAt(index);
			HSSFRow row = sheet.getRow(rowNum - 1);
			if (row == null) {
				return "";
			}
			HSSFCell cell = row.getCell(colNum);
			if (cell == null) {
				return "";
			}
			if (cell.getCellType() == CellType.STRING)
				return cell.getStringCellValue();
			else if (cell.getCellType() == CellType.NUMERIC)
				return String.valueOf(cell.getNumericCellValue());
			else if (cell.getCellType() == CellType.BLANK)
				return "";
			else
				return String.valueOf(cell.getBooleanCellValue());
		} catch (Exception e) {

			e.printStackTrace();
			return "";
		}
	}
}

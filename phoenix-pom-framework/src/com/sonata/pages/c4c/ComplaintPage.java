package com.sonata.pages.c4c;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.sonata.common.TestBase;
import com.sonata.objects.c4c.CaseComps;
import com.sonata.objects.c4c.LoginComps;

public class ComplaintPage extends TestBase {

	String loginClsName = "com.sonata.objects.c4c.LoginComps";
	String caseClsName = "com.sonata.objects.c4c.CaseComps";

	//ExtentReports report = localReport.get();

	public ComplaintPage(ThreadLocal<WebDriver> driver,
			ThreadLocal<ExtentReports> report, ThreadLocal<WebDriverWait> wait,
			org.apache.log4j.Logger logger) {
		super(driver, report, wait, logger);
	}
	
	public void click_OnCaseTab_SelectAllCases() {
		setClassName(loginClsName);
		performClick(LoginComps.C4C_Login_MainCaseTab);
		performClick(LoginComps.C4C_Login_SubCaseTab);
		setClassName(caseClsName);
		performClick(CaseComps.C4C_Case_ClickOnDropDownArrow);
		performClick(CaseComps.C4C_Case_AllCases);
	}

	public void applyFilter(String status, String complaintType) {
		setClassName(caseClsName);
		performClick(CaseComps.C4C_Case_Status);
		performEnterText(CaseComps.C4C_Case_Filter, status);
		performEnterKey(CaseComps.C4C_Case_Filter);
		performClick(CaseComps.C4C_Case_CaseType);
		performEnterText(CaseComps.C4C_Case_Filter, complaintType);
		performEnterKey(CaseComps.C4C_Case_Filter);
	}

	public void complaintPageVerification() {
		pageDisplay("CASE");
		setClassName(caseClsName);
		performGetText(CaseComps.C4C_Case_CaseID);
		boolean flag = isElementPresent(CaseComps.C4C_Case_CaseID);
		if (flag) {
			reportPassed();
		} else {
			reportFailure();
		}
	}

	public void createCase(String complType, String custName,
			String complCategory) {
		setClassName(loginClsName);
		performClick(LoginComps.C4C_Login_CloseHomeIcon);
		performClick(LoginComps.C4C_Login_MainCaseTab);
		performClick(LoginComps.C4C_Login_SubCaseTab);
		performClick(LoginComps.C4C_Login_CreateCase);
		performClick(LoginComps.C4C_Login_ComplaintTypeArrow);
		performSelectFromList(LoginComps.C4C_Login_ComplaintType, complType);
		performClick(LoginComps.C4C_Login_CustomerNameIconClick);
		performEnterText(LoginComps.C4C_Login_SearchCustomer, custName);
		performEnterKey(LoginComps.C4C_Login_SearchCustomer);
		performClick(LoginComps.C4C_Login_ClickCustomer);
		performEnterText(LoginComps.C4C_Login_ComplDescription, "sonata");
		performClick(LoginComps.C4C_Login_ComplCategoryArrow);
		performSelectFromList(LoginComps.C4C_Login_ComplaintCategory,
				complCategory);
		performClick(LoginComps.C4C_Login_ComplSaveAndOpen);
	}

	public void reopenReasonFieldVerification(String complaintID) {
		String expected = "Change of Resolution method|Further Inv. - all points not answered|";
		expected = expected
				+ "Further Inv. - with payment|Further Inv. - lack of info provided|Escalation|";
		expected = expected
				+ "Appreciation|Further Inv. - More Issues reported|Stop cheque/voucher";

		setClassName(loginClsName);
		performClick(LoginComps.C4C_Login_CloseHomeIcon);
		performClick(LoginComps.C4C_Login_MainCaseTab);
		performClick(LoginComps.C4C_Login_SubCaseTab);
		performClick(LoginComps.C4C_Login_SelectAllArrow);
		performClick(LoginComps.C4C_Login_SelectAllCase);
		performEnterText(LoginComps.C4C_Login_SearchCaseTextBox, complaintID);
		performEnterKey(LoginComps.C4C_Login_SearchCaseTextBox);
		LoginComps.C4C_Login_ComplaintID = createByObjectByReplacingGivenText(
				LoginComps.C4C_Login_ComplaintID, "1234", complaintID);
		performClick(LoginComps.C4C_Login_ComplaintID);
		performClick(LoginComps.C4C_Login_Edit);
		performClick(LoginComps.C4C_Login_StatusArrow);
		performSelectFromList(LoginComps.C4C_Login_SelectReopen, "Re Opened");
		scrollIntoViewElement(LoginComps.C4C_Login_ReopenReasonArrow);
		performClick(LoginComps.C4C_Login_ReopenReasonArrow);
		List<String> reopenReasonList = performMultipleGetText(LoginComps.C4C_Login_ReopenReasonText);
		synchronized (reopenReasonList) {
			StringBuffer buffer = new StringBuffer();
			for (int l = 0; l < reopenReasonList.size(); l++) {
				buffer.append(reopenReasonList.get(l).trim());
				if (l != (reopenReasonList.size() - 1)) {
					buffer.append("|");
				}
			}
			stringComparisonWithContains(expected, buffer.toString());
		}
	}
	
	

}

package com.sonata.utility;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.sonata.constants.Constants;

public class PropertiesReader {
	public static Logger logger1 = Logger.getLogger(PropertiesReader.class);
	
	public static Properties getProperty() {
		FileInputStream fis;
		Properties prop = null;
		try {
			fis = new FileInputStream(System.getProperty("user.dir")
					+ Constants.POM_PROPERTY_PATH);
			prop = new Properties();
			prop.load(fis);
			logger1.info("File exist and created property object:");
		} catch (Exception e) {
			logger1.error(e);
		}
		return prop;
	}
	
	public static Properties getProperty(String prptyFileName){
		FileInputStream fis;
		Properties prop = null;
		try {
			File fileName = new File(System.getProperty("user.dir")+"/resources/"
					+ prptyFileName);
			if(fileName.exists()){
			    fis = new FileInputStream(fileName);
			}else{
				fis = new FileInputStream(prptyFileName);
			}
			prop = new Properties();
			prop.load(fis);
			logger1.info("File exist and created property object:");
		} catch (Exception e) {
			logger1.error(e);
		}
		return prop;	
	}

}
